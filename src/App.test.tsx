import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react';
import React from 'react';
import App from './App';

test('renders todo list title', () => {
  render(<App />);
  const linkElement = screen.getByText(/Todos/i);
  expect(linkElement).toBeInTheDocument();
});
