import React from 'react';
import './App.css';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';

import { TodoProvider } from './todos/todoContext';
import { ManageTodosSection } from './sections';





// todo extract theme into its own useTheme hook
// todo: change todos to implement a useReducer hooks

const theme = createTheme();

export default function SignIn() {

  return (
    <ThemeProvider theme={theme}>
      <TodoProvider>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
         <ManageTodosSection />
        </Box>
  
      </Container>
      </TodoProvider>
    </ThemeProvider>
  );
}