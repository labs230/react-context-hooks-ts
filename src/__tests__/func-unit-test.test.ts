import { testGreeting } from '../controller'

describe("App testing with Jest", () => {

    it('Should return some text', () => {
        expect(testGreeting('Josh')).toEqual("Hi my name is Josh")
    })
})