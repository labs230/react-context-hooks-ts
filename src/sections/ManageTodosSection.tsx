import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import useTodos from "../todos/useTodos";

import TodoList from '../todos/TodoList';

const ManageTodosSection: React.FunctionComponent = () => {
  const inputRef = React.createRef<HTMLInputElement>();
  const { setTodos } = useTodos();

  return (
    <main>

    <Typography component="h1" variant="h5">
            Todos
          </Typography>

          <Box sx={{ mt: 1 }}>
          <TodoList />
            <TextField
              inputRef={inputRef}
              onKeyDown={({ key }) => setTodos(inputRef, key)} 
              margin="normal"
              required
              fullWidth
              id="add-todo"
              label="Add Todo"
              name="todos"
              autoFocus
            />
          
            <Button
              onClick={() => {
                console.log('clicked')
                console.log(inputRef)
                console.log(inputRef?.current?.value)
                setTodos(inputRef)
              }}
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Set Todo
            </Button>
            
          </Box>
          </main>
  );
};


export default ManageTodosSection;
