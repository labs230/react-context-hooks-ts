import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import useTodos from "./useTodos";

const TodoList: React.FunctionComponent = (): React.ReactElement => {
  const { todos } = useTodos();
  return (
    <List>
      {todos?.map((todo, i) => (
        <ListItem key={`${todo}-${i}`}>{todo}</ListItem>
      ))}
    </List>
  );
};

export default TodoList;
