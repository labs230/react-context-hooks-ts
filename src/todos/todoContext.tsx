import * as React from "react";

export type ProviderProps = {
  todos: Array<string>;
  setTodos: (inputRef: React.RefObject<HTMLInputElement>, key?: string) => void; // React.Dispatch<React.SetStateAction<string[]>>;
};

export const TodoContext = React.createContext<ProviderProps>({
  todos: [],
  setTodos: () => null,
});

// todo: change todo to reducer instead

export const TodoProvider: React.FunctionComponent<{
  children: React.ReactNode;
}> = ({ children }) => {
  const [todos, setTodos] = React.useState<string[]>([]);

  // create setTodos handler
  const setTodosHandler: ProviderProps["setTodos"] = (inputRef, key) => {
    if (inputRef?.current?.value?.length && (!key || key === "Enter")) {
      const val = inputRef.current?.value as string;
      setTodos((prevSt) => [...prevSt, val]);
      inputRef.current.value = "";
    }
  };

  return (
    <TodoContext.Provider
      value={{
        todos: todos,
        setTodos: setTodosHandler,
      }}
    >
      {children}
    </TodoContext.Provider>
  );
};
