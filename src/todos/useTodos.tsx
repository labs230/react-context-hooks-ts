import { useContext } from "react";
import { TodoContext } from "../todos/todoContext";

const useTodos = () => {
  const ctx = useContext(TodoContext);

  if (!ctx)
    throw new Error(
      "No context found. Please ensure the Provider is wrapping the App"
    );

  const { todos, setTodos } = ctx;

  return { todos, setTodos };
};

export default useTodos;
